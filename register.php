<?php
require_once('Models/Connection.php');
$conn = new Connection();
$firstNameError = $lastNameError = $emailError = $passwordError ="";
$firstName = $lastName = $email = $password = "";
if($_SERVER["REQUEST_METHOD"] == "POST"){
    
  if (empty($_POST["firstName"]) || empty($_POST["lastName"]) || empty($_POST['email']) || empty($_POST['password'])) {
      echo "<script>alert('Please fill out all the fields');</script>";
  }
  else
  {
    $firstName = $_POST["firstName"];
    $lastName = trim($_POST['lastName']);
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
  
    if(!preg_match("/[a-zA-Z]$/",$firstName)) {
      print_r($firstName);die;
      echo"<script>alert('Wrong Username format!! You can only use letters and numbers in firstName!!');</script>";
    }
    elseif(!preg_match("/[a-zA-Z]$/",$lastName)) {
      echo"<script>alert('Wrong Username format!! You can only use letters and numbers in lastName!!');</script>";
    }
    elseif(!preg_match ('/^(?=.*[0-9])(?=.*[A-Z]).{6,12}$/',$password)){
      echo"<script>alert('Wrong Password Format!! The Password should be 6-12 characters long.');</script>";     
    }
    elseif(!preg_match ('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i',$email)){
      echo"<script>alert('Wrong email format');</script>";
    }
    else{
      echo "<script>alert('Register  Successful');</script>";
    } 
  }
  $conn->sql = "INSERT INTO members(first_name, last_name, email, password) VALUES('$firstName', '$lastName', '$email', '$password')";
        $conn->res = mysqli_query($conn->conn, $conn->sql)
                        or die($conn->error = mysqli_error($conn->conn));
        $conn->affRows = mysqli_affected_rows($conn->conn);
        if($conn->affRows>0){
        echo "test";
         echo"<script>window.location='login.php'</script>";
        }
        else
          echo"<script>window.location='register.php'</script>";
      
          
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>
      Assignment-system
    </title>
    <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
    <link href="public/assets/stylesheets/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="public/assets/stylesheets/font-awesome.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="public/assets/stylesheets/hightop-font.css" media="all" rel="stylesheet" type="text/css" />
    <link href="public/assets/stylesheets/style.css" media="all" rel="stylesheet" type="text/css" />
  </head>
  <body class="login2">
    <!-- Signup Screen -->
    <div class="login-wrapper">
      <h1>
        <a href="./">ASSIGENMENT SYSTEM</a>
      </h1>
      <form method="post" action="<?php trim($_SERVER['PHP_SELF']) ?>">
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"></span>
            <input class="form-control" type="text" placeholder="Enter your First Name" name="firstName">
          </div>
        </div>
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"></i></span>
            <input class="form-control" type="text" placeholder="Enter your Last Name" name="lastName">
          </div>
        </div>
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
            <input class="form-control" type="text" placeholder="Enter your email address" name="email">
          </div>
        </div>
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-asterisk"></i></span>
            <input class="form-control" type="password" placeholder="Enter Password" name="password">
          </div>
        </div>
        <div class="form-group">
        <input class="btn btn-lg btn-primary btn-block" type="submit" value="Sign up">
        </div>
        <p>
          Already have an account?
        </p>
        <a class="btn btn-default-outline btn-block" href="login.php">Login now</a>
      </form>
    </div>
    <!-- End Signup Screen -->
  </body>
</html>