<?php
$GLOBALS['count_array'] = [
                              'id' => 243,
                              'description' => 'apples',
                              'type' => 'bramley',
                            ];
  // session_start();
  if(isset($_POST['form-submit1']))
    checkStringLength();
  elseif(isset($_POST['form-submit2']))
    changeToUpperCase();
  elseif(isset($_POST['form-submit3']))
    findStringPosition();
  elseif(isset($_POST['form-submit4']))
    encryptString();
  elseif(isset($_POST['form-submit5']))
    getDataType();
  elseif(isset($_POST['form-submit6']))
    checkNumeric();
  elseif(isset($_POST['form-submit7']))
    roundofNumber();
  elseif(isset($_POST['form-submit8']))
    productArray();
  elseif(isset($_POST['input-submit9']))
    countArray();
  elseif(isset($_POST['input-submit10']))
    checkArray();
  elseif(isset($_POST['input-submit11']))
    addPriceArray();
  elseif(isset($_POST['input-submit12']))
    explodeArray();
  elseif(isset($_POST['input-submit13']))
    todaydate();
  elseif(isset($_POST['input-submit14']))
    calculateAge();
  elseif(isset($_POST['input-submit15']))
    sanitization();
  elseif(isset($_POST['input-submit16']))
    checkString();


  /*function to check length of the string */

  function checkStringLength()
  {
    if(!empty($_POST['form-string']))
    {
      $length = strlen($_POST['form-string']);
      $_SESSION["length"] = $length;
    }
    else
    {
      $_SESSION["error"] = "Please enter a string";
    }
  }

  /*function to convert the letters to uppercase*/

  function changeToUpperCase()
  {
    if(!empty($_POST['string-to-convert']))
    {
      $convert = strtoupper($_POST['string-to-convert']);
      $_SESSION["uppercase"] = $convert;
    }
    else
    {
      $_SESSION["error1"] = "Please enter a string";
    }
  }


  /*function to find the position of string*/


  function findStringPosition()
  {
    $string_position = $_POST['string-to-find'];
    $string_query = $_POST['input-find-query'];
    if(!empty($string_position) && !empty($string_query))
    {
      $res = strpos($string_position, $string_query);
      $_SESSION["str-pos"] = $res;
    }
    else
    {
      $_SESSION["error3"] = "Please enter some string";
    }
  }

  /*function To encript string */

  function encryptString()
  {
    $to_encrypt = $_POST['input-encrypt'];
    if(!empty($to_encrypt))
    {
      $encrypt = md5($to_encrypt);
      $_SESSION['encrypt'] = $encrypt;
    }
    else
    {
      $_SESSION["error4"] = "Please enter a string";
    }
  }

// function to get data type

  function getDataType()
  {
    $getvariable = $_POST['input-data'];
    if(!empty($getvariable))
    {
      if(filter_var($getvariable, FILTER_VALIDATE_INT))
        $_SESSION['data'] = "integer";
      elseif(filter_var($getvariable, FILTER_VALIDATE_FLOAT))
        $_SESSION['data'] = "float";
      else
        $_SESSION['data'] = "string";

    }
    else
    {
      $_SESSION['error5'] = "Please enter a string";
    }
  }


// function to check numeric or not?

  function checkNumeric()
  {
    $getnumber = $_POST['input-number'];
    if(!empty($getnumber))
    {
      if(is_numeric($getnumber))
        $_SESSION['number'] = "numeric";
      else
      {
        $_SESSION['error6'] = "please enter the number";
      }
    }
  }

// function to round of number

  function roundofNumber()
  {
    $getdecimal= $_POST['input-decimal'];
    if(!empty($getdecimal))
    {
      $rounded = round($getdecimal,1);
      $_SESSION['decimal'] = $rounded;
     }
     else
     {
      $_SESSION['error7'] = "Please enter valid dacimal number";
     }
  }

  // function to find product array

  function productArray()
  {
    $id = $_POST['input-id'];
    $description = $_POST['input-description'];
    $type = $_POST['input-type'];
    $product_array = [];
    if(empty($id) || empty($description) || empty($type))
    {
      $_SESSION['error8'] = "Please enter all the values";
    }
    else
    {
      $product_array = [
        'id' => $id,
        'description' => $description,
        'type' => $type
      ];
      $_SESSION['array_result'] = $product_array;
    }
  }

// function to count array

  function countArray()
  {
    $length = count($GLOBALS['count_array']);
    $_SESSION['array_length'] = $length;
  }

//  function to check array

  function checkArray()
  {
    $string = $_POST['array_not'];
    if(empty($string))
    {
      $_SESSION['error9'] = "Please enter the string";
    }
    else
    {
      if(in_array($string, $GLOBALS['count_array']))
      {
        $_SESSION['is_in_array'] = "1 (true i.e. it exists)";
      }
      else
      {
        $_SESSION['is_in_array'] = "0 (false i.e. doesn't exist)";
      }
    }
  }

  // function to push into existing array

  function addPriceArray()
  {
    $price = $_POST['input-price'];
    if(empty($price))
    {
      $_SESSION['error10'] = "Please enter price";
    }
    else
    {
      $to_push = ['price' => $price];
      $pushed = array_merge($GLOBALS['count_array'], $to_push);
      $_SESSION['add_array'] = $pushed;
    }
  }

//  function to explode array

  function explodeArray()
  {
    $url = $_POST['input-url'];
    if(empty($url))
    {
      $_SESSION['error12'] = "Please enter URL";
    }
    else
    {
      $_SESSION['explode_url'] = explode('.', $url);
    }
  }

//  function to display todays date

  function Todaydate()
  {
    $_SESSION['date'] = date('D jS F Y');
  }

//  function to calculate age

  function calculateAge()
  {
    $date = $_POST['input-dob'];
    if(empty($date))
    {
      $_SESSION['error14'] = "Please enter date";
    }
    else
    {
      $dob = new DateTime($date);
      $now = new DateTime();
      $difference = $now->diff($dob);
      $age = $difference->y;
      $_SESSION['calc_age'] = $age;
    }
  }

  function sanitization()
  {
    if(empty($_POST['sanitization']))
    {
      $_SESSION['error15'] = "Please enter something";
    }
    else
    {
      $string1 = $_POST['sanitization'];
      $_SESSION['sanitize'] = htmlspecialchars($string1);
    }
  }

  function checkString()
  {
    if(empty($_POST['input-check-string']))
    {
      $_SESSINO['error16'] = "Please enter something";
    }
    else
    {
      $string1 = $_POST['input-check-string'];
      if(ctype_alnum($string1))
      {
        $_SESSION['checking'] = "The string consists of alphanumeric characters only";
      }
      else
      {
        $_SESSION['checking'] = "Error!!";
      }
    }
  }

?>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info m-20" style="padding:15px; font-size:20px">
          <strong>Check</strong> String
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:190px;" >
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group">
              <div class="col-12 col-md-9">
                <label for="length-check">Enter string to check length of</label>
                <input type="text" class="form-control" placeholder="String eg. apple" id="length-check" name="form-string" style="margin:12px">
              </div>
              </div>
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="form-submit1">
              <i class="glyphicon glyphicon-ok"></i> Check Length
            </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset1">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
          <span class="result"><?php echo isset($_SESSION["length"]) ? "The length of the string is:" .$_SESSION["length"] : @$_SESSION["error"]; ?></p></span>
        </div>
      </div>
    </div>
    <br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info" style="padding:15px; font-size:20px">
          <strong>Change all characters</strong> to Uppercase
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:190px;">
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group">
              <div class="col-12 col-md-9">
                <label for="convert">Enter string </label>
                <input type="text" class="form-control" name="string-to-convert" placeholder="String eg. apple" id="convert"style="margin:13px"
                value="<?php if(isset($_SESSION['uppercase']))
                                echo $_SESSION['uppercase'];
                        ?>">
              </div>
              </div>
              <span class="result"><?php echo isset($_SESSION["error1"]) ? $_SESSION["error1"] : '' ?></span>
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="form-submit2">
            <i class="glyphicon glyphicon-ok"></i> Change to Uppercase
          </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset2">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
        </div>
      </div>
    </div>
    <br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info" style="padding:15px; font-size:20px">
          <strong>Encrypt</strong> a string with md5
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:190px;">
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group custom-div">
              <div class="col-12 col-md-9">
                <label for="string-to-encrypt">Enter string to encrypt </label>
                <input type="text" class="form-control" name="input-encrypt" placeholder="String eg. apple" id="string-to-encrypt" style="margin:13px">
              </div>
              </div>
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="form-submit4">
            <i class="glyphicon glyphicon-ok"></i> Encrypt
          </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset3">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
          <span class="result"><?php echo isset($_SESSION["encrypt"]) ? "The encrypted string is " .$_SESSION["encrypt"] : @$_SESSION["error4"] ?></span>

        </div>
      </div>
    </div>
    <br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info" style="padding:15px; font-size:20px">
          <strong>Get</strong> data type of a variable
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:190px;">
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group custom-div">
              <div class="col-12 col-md-9">
                <label for="check-data">Enter data  </label>
                <input type="text" class="form-control" name="input-data" placeholder="Any data eg. 19" id="check-data" style="margin:13px">
              </div>
              </div>
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="form-submit5">
            <i class="glyphicon glyphicon-ok"></i> Get data type
          </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset4">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
         <span class="result"><?php echo isset($_SESSION["data"]) ? "The data type of " .$_POST['input-data'] ." is " .$_SESSION["data"] : @$_SESSION["error5"] ?></span>

        </div>
      </div>
    </div>
    <br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info" style="padding:15px; font-size:20px">
          <strong>Find position</strong> of string
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:230px;">
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group custom-div">
              <div class="col-12 col-md-9">
                <label for="find">Enter string to search in </label>
                <input type="text" class="form-control" name="string-to-find" placeholder="String eg. www.somesite.com" id="find" style="margin:13px">
                <label for="find-what">Enter what to find ?</label>
                <input type="text" class="form-control" id="find-what" name="input-find-query" placeholder="Eg. some" style="margin:13px">
              </div>
              </div>
              <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="form-submit3">
            <i class="glyphicon glyphicon-ok"></i> Find
          </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset2">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
          <span class="result"><?php echo isset($_SESSION["str-pos"]) ? "The string starts at index " .$_SESSION["str-pos"] : @$_SESSION["error3"] ?></span>

        </div>
      </div>
    </div>
    <br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info" style="padding:15px; font-size:20px">
          <strong>Check</strong> numeric or not?
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:190px;">
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group custom-div">
              <div class="col-12 col-md-9">
                <label for="string-to-encrypt">Enter the number </label>
                <input type="text" class="form-control" name="input-number" placeholder="Enter Number" id="true-false" style="margin:13px">
              </div>
              </div>
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="form-submit6">
            <i class="glyphicon glyphicon-ok"></i> Check Numeric
          </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset6">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
          <span class="result"><?php echo isset($_SESSION["number"]) ? "The value is " .$_SESSION['number'] : @$_SESSION["error6"] ?></span>
        </div>
      </div>
    </div>
    <br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info" style="padding:15px; font-size:20px">
          <strong>Round of</strong> to 1 decimal place
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:190px;">
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group custom-div">
              <div class="col-12 col-md-9">
                <label for="round-of">Enter the number </label>
                <input type="text" class="form-control" name="input-decimal" placeholder="decimal number eg. 27.579" id="round-of" style="margin:13px">
              </div>
              </div>
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="form-submit7">
            <i class="glyphicon glyphicon-ok"></i> Round of
          </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset7">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
          <span class="result"><?php echo isset($_SESSION["decimal"]) ? "The round of number is " .$_SESSION["decimal"] : @$_SESSION["error7"] ?></span>
        </div>
      </div>
    </div>
    <br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info" style="padding:15px; font-size:20px">
          <strong>Print</strong> the product array
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:350px;">
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group custom-div">
              <div class="col-12 col-md-9">
                <label for="array">Enter  </label>
                <input type="array" class="form-control" name="input-id" placeholder="#" id="array" style="margin:13px">
                <label for="array1">Enter</label>
                <input type="text" class="form-control" id="array1" name="input-description" placeholder="Eg. some" style="margin:13px">
                <label for="array2">Enter  </label>
                <input type="text" class="form-control" id="array2" name="input-type" placeholder="Eg. some" style="margin:13px">
              </div>
              </div>
              <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="form-submit8">
            <i class="glyphicon glyphicon-ok"></i> Find
          </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset8">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
          <span class="result"><?php isset($_SESSION["array_result"]) ? print_r($_SESSION["array_result"]) : @print_r($_SESSION["error8"]) ?></span>
        </div>
      </div>
    </div>
    <br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info m-20" style="padding:15px; font-size:20px">
          <strong>Count</strong> the number in array
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:190px;" >
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group">
              <div class="col-12 col-md-9">
                <label for="count-array">Input</label>
                <input type="text" class="form-control" placeholder="input" id="count-array" name="count_array" style="margin:12px">
              </div>
              </div>
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="input-submit9">
              <i class="glyphicon glyphicon-ok"></i> Check Length
            </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset9">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
          <span class="result"><?php echo isset($_SESSION["array_length"]) ? "The no if item in array is:" .$_SESSION["array_length"] : @$_SESSION["error"]; ?></p></span>

        </div>
      </div>
    </div>
    <br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info m-20" style="padding:15px; font-size:20px">
          <strong>Check</strong> input value is in array or not?
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:190px;" >
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group">
              <div class="col-12 col-md-9">
                <label for="is-array">Input</label>
                <input type="text" class="form-control" placeholder="input" id="is-array" name="array_not" style="margin:12px">
              </div>
              </div>
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="input-submit10">
              <i class="glyphicon glyphicon-ok"></i> Check
            </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset1">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
          <span class="result"><?php echo isset($_SESSION["is_in_array"]) ? "The no if item in array is:" .$_SESSION["is_in_array"] : @$_SESSION["error"]; ?></p></span>

        </div>
      </div>
    </div>
    <br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info m-20" style="padding:15px; font-size:20px">
          <strong>Add</strong> price
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:190px;" >
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group">
              <div class="col-12 col-md-9">
                <label for="price-add">Input</label>
                <input type="text" class="form-control" placeholder="input" id="price-add" name="input-price" style="margin:12px">
              </div>
              </div>
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="input-submit11">
              <i class="glyphicon glyphicon-ok"></i> Check
            </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset11">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
          <span class="result"><?php isset($_SESSION["add_array"]) ? print_r($_SESSION["add_array"]) : @$_SESSION["error10"]; ?></span>
        </div>
      </div>
    </div>
    <br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info m-20" style="padding:15px; font-size:20px">
          <strong>Explode</strong> URL in array
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:190px;" >
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group">
              <div class="col-12 col-md-9">
                <label for="url-exp">Input</label>
                <input type="text" class="form-control" placeholder="input" id="url-exp" name="input-url" style="margin:12px">
              </div>
              </div>
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="input-submit12">
              <i class="glyphicon glyphicon-ok"></i> Enter URl
            </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset12">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
          <span class="result"><?php isset($_SESSION["explode_url"]) ? print_r($_SESSION["explode_url"]) : @$_SESSION["error12"]; ?></span>
        </div>
      </div>
    </div>
    <br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info m-20" style="padding:15px; font-size:20px">
          <strong>Todays</strong> date
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:190px;" >
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="input-submit13">
              <i class="glyphicon glyphicon-ok"></i> Todays Date
            </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset13">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
          <span class="result"><?php echo isset($_SESSION["date"]) ? "Todays date is : " .$_SESSION["date"] : @$_SESSION["error13"]; ?></span>
        </div>
      </div>
    </div>
    <br>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info m-20" style="padding:15px; font-size:20px">
          <strong>Calculate</strong> age
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:190px;" >
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group">
              <div class="col-12 col-md-9">
                <label for="age">Input</label>
                <input type="date" class="form-control" placeholder="day/month/year" id="age" name="input-dob" style="margin:12px">
              </div>
              </div>
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="input-submit14">
              <i class="glyphicon glyphicon-ok"></i> Calculate
            </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset14">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
          <span class="result"><?php echo isset($_SESSION["calc_age"]) ? "your age is: " .$_SESSION["calc_age"] : @$_SESSION["error14"]; ?></span>
        </div>
      </div>
    </div>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info m-20" style="padding:15px; font-size:20px">
          <strong>sanitisation </strong>
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:190px;" >
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group">
              <div class="col-12 col-md-9">
                <label for="sanitize">Input</label>
                <input type="text" class="form-control" placeholder="input" id="sanitize" name="sanitization" style="margin:12px">
              </div>
              </div>
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="input-submit15">
              <i class="glyphicon glyphicon-ok"></i> Check
            </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset15">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          <span class="result"><?php isset($_SESSION['sanitize']) ?  print_r($_SESSION['sanitize']) : print_r(@$_SESSION['error14']) ?></span>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info m-20" style="padding:15px; font-size:20px">
          <strong>Alpha Numeric Check </strong>
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:190px;" >
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group">
              <div class="col-12 col-md-9">
                <label for="sanitize">Input</label>
                <input type="text" class="form-control" placeholder="input" id="sanitize" name="input-check-string" style="margin:12px">
              </div>
              </div>
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="input-submit16">
              <i class="glyphicon glyphicon-ok"></i> Check
            </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset15">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          <span class="result"><?php isset($_SESSION['checking']) ?  print_r($_SESSION['checking']) : print_r(@$_SESSION['error15']) ?></span>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
    // session_destroy();
    // $_SESSION = [];
?>