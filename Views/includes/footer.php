<script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/raphael.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/selectivizr-min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/jquery.mousewheel.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/jquery.vmap.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/jquery.vmap.sampledata.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/jquery.vmap.world.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/jquery.bootstrap.wizard.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/fullcalendar.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/gcal.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/datatable-editable.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/jquery.easy-pie-chart.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/excanvas.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/jquery.isotope.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/isotope_extras.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/modernizr.custom.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/select2.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/styleswitcher.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/wysiwyg.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/typeahead.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/summernote.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/jquery.inputmask.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/bootstrap-fileupload.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/bootstrap-timepicker.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/bootstrap-colorpicker.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/typeahead.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/spin.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/ladda.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/moment.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/mockjax.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/bootstrap-editable.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/xeditable-demo-mock.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/xeditable-demo.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/address.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/daterange-picker.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/date.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/morris.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/skycons.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/fitvids.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/dropzone.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/jquery.nestable.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/main.js" type="text/javascript"></script>
    <script src="<?php echo BASE_URL; ?>/public/assets/javascripts/respond.js" type="text/javascript"></script>
            </div>
        </div>
    </body>
</html>