<?php
  require_once('Models/Connection.php');
  $email = $password = "";
  if(isset($_POST['input-submit']))
  {
  //  echo "string";die;
    if(empty($_POST['input-email']) || empty($_POST['input-password']))
    {
      // echo "string1";die;
      echo "<script>alert('Please enter all the details');</script>";
    }
    else
    {
      $email = test($_POST['input-email']);
      $password = test($_POST['input-password']);
      $user = check($email, $password);
      // print_r($user);die;
      if($user)
      {
        $_SESSION['user_details'] = $user;
        echo "<script>window.location='?pages=welcome'</script>";
      }
      else
      {
        $_SESSION['login_error'] = "Invalid Credentials!!";
        echo "<script>window.location='?pages=login'</script>";
      }
    }

  }

  function check($email, $psw)
  {
    // echo "I am here";die;
    $conn = new Connection();
    $conn->sql = "SELECT * FROM members WHERE email='$email' AND password='$psw'";
    $conn->res = mysqli_query($conn->conn, $conn->sql)
                            or die($conn->error=mysqli_error($conn->conn));
    // print_r($conn->res);die;
      $conn->numRows = mysqli_num_rows($conn->res);
      if($conn->numRows>0)
      {
        while($row=mysqli_fetch_object($conn->res))
        {
          array_push($conn->data, $row);
        }
        return $conn->data;
      }
  }


  function test($data)
  {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
  ?>
<!DOCTYPE html>
<html>
  <head>
    <title>
      Assignment-system
    </title>
    <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
    <link href="public/assets/stylesheets/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="public/assets/stylesheets/font-awesome.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="public/assets/stylesheets/hightop-font.css" media="all" rel="stylesheet" type="text/css" />
    <link href="public/assets/stylesheets/style.css" media="all" rel="stylesheet" type="text/css" />

  </head>
  <body class="login2">
    <!-- Login Screen -->
    <div class="login-wrapper">
      <h1>
        <a href="./">ASSIGNMENT SYSTEM</a>
      </h1>
      <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input class="form-control" name="input-email" placeholder="Email" type="text">
          </div>
        </div>
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-asterisk"></i></span><input class="form-control" name="input-password" placeholder="Password" type="password">
          </div>
        </div>
        <input class="btn btn-lg btn-primary btn-block" name="input-submit" type="submit" value="Log in">
      </form>
      <p>
        Don't have an account yet?
      </p>
      <a class="btn btn-default-outline btn-block" href="register.php">Sign up now</a>
    </div>
    <!-- End Login Screen -->
  </body>
</html>