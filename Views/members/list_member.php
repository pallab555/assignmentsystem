<?php
    require_once('Models/Connection.php');
    $list_members = getAllMembers();
    function getAllMembers()
    {
        $conx = new Connection();
        $conx->sql = "SELECT * FROM members";
        $conx->res = mysqli_query($conx->conn, $conx->sql)
                            or die($conx->error = mysqli_error($conx->conn));
        $conx->numRows = mysqli_num_rows($conx->res);
        if($conx->numRows>0)
        {
            while($row = mysqli_fetch_object($conx->res))
            {
                array_push($conx->data, $row);
            }
            return $conx->data;
        }
    }
?>
    <div class="row">
          <div class="col-lg-12">
            <div class="widget-container fluid-height clearfix">
              <div class="heading">
                <i class="fa fa-table"></i>Members List Table
              </div>
              <div class="widget-content padded clearfix">
                <table class="table table-bordered table-striped" id="dataTable1">
                  <thead>
                    <th class="check-header hidden-xs">
                      <label><input id="checkAll" name="checkAll" type="checkbox"><span></span></label>
                    </th>
                    <th>
                      First Name
                    </th>
                    <th>
                      Last Name
                    </th>
                    <th class="hidden-xs">
                      Email
                    </th>
                    <th class="hidden-xs">
                      Password
                    </th>
                    <th class="hidden-xs">
                      Action
                    </th>
                  </thead>
                  <tbody>
                  <?php foreach($list_members as $members) { ?>
                    <tr>
                      <td class="check hidden-xs">
                        <label><input name="optionsRadios1" type="checkbox" value="option1"><span></span></label>
                      </td>
                      <td>
                        <?php echo $members->first_name; ?>
                      </td>
                      <td>
                        <?php echo $members->last_name; ?>
                      </td>
                      <td class="hidden-xs">
                        <?php echo $members->email; ?>
                      </td>
                      <td class="hidden-xs">
                        <?php echo $members->password; ?>
                      </td>
                      <td class="actions">
                        <div class="action-buttons">
                          <a href="<?php echo BASE_URL; ?>?pages=members&action=edit&id=<?php echo $members->id; ?>"><span class="glyphicon glyphicon-edit"></span></a>&nbsp;&nbsp;<a href="<?php echo BASE_URL; ?>?pages=members&action=delete&id=<?php echo $members->id ?>"><span class="glyphicon glyphicon-trash"></span></a>
                        </div>
                      </td>
                    </tr>
                    <?php } ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>