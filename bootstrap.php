<?php

  session_start();
  $pages = isset($_GET['pages']) ? $_GET['pages'] : '';
  // print_r($menu);die;
  $action = isset($_GET['action']) ? $_GET['action'] : '';

  switch($pages){
    case "test": if($action=="initial")
                    $page = "Views/Test/initial.php";
                  elseif($action=="function")
                    $page = "Views/Test/function_test.php";
                  elseif($action=="members")
                    $page = "Views/Test/addmembers.php";
                  elseif($action=="formdb")
                    $page = "Views/Test/form-db.php";
                  else
                    $page = "Views/Test/oo.php";
                    break;
    case "members": if($action=="add") {
                      $page = "Views/members/addmember.php";
                    }
                    elseif($action=="list")
                    {
                      $page = "Views/members/list_member.php";
                    }
                    elseif($action=="delete")
                      $page = "Views/members/delete_member.php";
                    elseif($action=="edit")
                      $page = "Views/members/edit_member.php";
                    else{
                      $page = "not found";
                    }
                    break;
    case "xmljson": if($action=="xml")
                      $page = "xml.php";
                    elseif($action=="json")
                      $page="json.php";
                    else{
                      $page="not found";
                    }
                    break;
    case "welcome": $page = "Views/Welcome.php";
                    break;
    case "login": $page = "Views/login.php";
                  break;
    case "register": $page = "register.php";
                      break;
    case "logout": session_destroy();
                    $page = "Views/login.php";
    default: $page = "Views/Welcome.php";
                      break;
  }
 ?>