<?php
    class Connection
    {
        public $host;
        public $user;
        public $password;
        public $db;

        public $sql;
        public $res;
        public $conn;
        public $error;

        public $numRows;
        public $affRows;

        public $data = [];

        public function __construct($db="assigenment", $u="root", $p="password", $h="localhost")
        {
            $this->host = $h;
            $this->user = $u;
            $this->password = $p;
            $this->db = $db;

            $this->conn = mysqli_connect($this->host, $this->user, $this->password, $this->db)
                                    or die($this->error = mysqli_error($this->conn));
        }
    }

    $obj = new Connection;
?>