<?php
    require_once('Models/Connection.php');
    $obj = new Connection();

    $db_params = [
        'hostname' => $obj->host,
        'username' => $obj->user,
        'password' => $obj->password,
        'database' => $obj->db,
        'db_driver' => PDO::getAvailableDrivers()[0]
    ];
    echo '<pre>';
    print_r($db_params);
    echo '</pre>';
    ?>