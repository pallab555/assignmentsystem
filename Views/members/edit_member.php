<?php
    require_once('Models/Connection.php');

    $id = isset($_GET['id']) ? $_GET['id'] : '';

    $member_by_id = getUserById($id);
    if(isset($_POST['input-submit']))
    {

            $firstname = $_POST['input-firstname'];
            $lastname = $_POST['input-lastname'];
            $email = $_POST['input-email'];
            $password = $_POST['input-password'];
            $member_id = $_POST['input-id'];

            $objConn = new Connection();
            $objConn->sql = "UPDATE members SET first_name='$firstname',last_name='$lastname',email='$email',password='$password' WHERE id='$member_id'";
            $objConn->res = mysqli_query($objConn->conn, $objConn->sql)
                                    or die($objConn->error = mysqli_error($objConn->conn));

            $objConn->affRows = mysqli_affected_rows($objConn->conn);
            if($objConn->affRows>0)
                echo "<script>window.location='?pages=members&action=list'</script>";
            // else
                // echo "string";die;

    }

    function getUserById($id)
    {
        $objConn = new Connection();
        $objConn->sql = "SELECT * FROM members WHERE id='$id'";
        $objConn->res = mysqli_query($objConn->conn, $objConn->sql)
                                or die($objConn->error=mysqli_error($objConn->conn));
        $objConn->numRows = mysqli_num_rows($objConn->res);
        if($objConn->numRows>0)
        {
            while($row=mysqli_fetch_object($objConn->res))
            {
                array_push($objConn->data, $row);
            }
            return $objConn->data;
        }
    }


?>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="../../public/assets/styles.css">
  </head>

  <h1>Edit your Profile</h1>
  <form method="post" action="<?php trim($_SERVER['PHP_SELF']) ?>">
      <?php foreach($member_by_id as $members) { ?>
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon"></span>
              <input class="form-control" type="text" placeholder="Enter your First Name" name="input-firstname" value="<?php echo $members->first_name; ?>">
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"></i></span>
            <input class="form-control" type="text" placeholder="Enter your Last Name" name="input-lastname" value="<?php echo $members->last_name; ?>">
          </div>
        </div>
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
            <input class="form-control" type="text" placeholder="Enter your email address" name="input-email" value="<?php echo $members->email; ?>">
          </div>
        </div>
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-asterisk"></i></span>
            <input class="form-control" type="password" placeholder="Enter Password" name="input-password" value="<?php echo $members->password; ?>">
          </div>
          <div class="input-group">
              <input type="hidden" name="input-id" value="<?php echo $members->id; ?> ">
          </div>
        </div>
        <input class="btn btn-lg btn-primary btn-block" type="submit" value="Update" name="input-submit">
    <?php } ?>
      </form>
