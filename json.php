<?php 
	/*Telling browser to expect json*/
	header("Content-type: application/json");

	/*Database connection*/
	$conn=mysqli_connect("localhost", "root", "", "assigenment");

	$query = "SELECT * FROM assignment";
	$result = mysqli_query($conn, $query) or die("Error in query: $query. ".mysql_error());

	/*saving the table data in array*/
	$arRows = array();
	while ($row_result = mysqli_fetch_assoc($result)) {
	  array_push($arRows, $row_result);
	}

	/*printing the data as json*/
	echo json_encode($arRows);

?>