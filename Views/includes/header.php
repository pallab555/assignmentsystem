<!DOCTYPE html>
<html>
  <head>
    <title>
      Assignment Tracker - Dashboard
    </title>
    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/font-awesome.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/hightop-font.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/isotope.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/jquery.fancybox.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/wizard.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/fullcalendar.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/select2.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/morris.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/datatables.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/datepicker.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/timepicker.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/colorpicker.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/bootstrap-switch.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/bootstrap-editable.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/daterange-picker.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/typeahead.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/summernote.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/ladda-themeless.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/social-buttons.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/jquery.fileupload-ui.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/dropzone.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/nestable.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/pygments.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/style.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/color/green.css" media="all" rel="alternate stylesheet" title="green-theme" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/color/orange.css" media="all" rel="alternate stylesheet" title="orange-theme" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/color/magenta.css" media="all" rel="alternate stylesheet" title="magenta-theme" type="text/css" />
    <link href="<?php echo BASE_URL; ?>/public/assets/stylesheets/color/gray.css" media="all" rel="alternate stylesheet" title="gray-theme" type="text/css" />

    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
  </head>
  <body class="page-header-fixed bg-1">
    <div class="modal-shiftfix">
      <!-- Navigation -->
      <div class="navbar navbar-fixed-top scroll-hide">
        <div class="container-fluid top-bar">
          <div class="pull-right">
            <ul class="nav navbar-nav pull-right">
              <li class="dropdown user hidden-xs"><a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img width="34" height="34" src="<?php echo BASE_URL; ?>/public/assets/images/me.jpg" />Pallab Kaphle<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="#">
                    <i class="fa fa-user"></i>My Account</a>
                  </li>
                  <li><a href="#">
                    <i class="fa fa-gear"></i>Account Settings</a>
                  </li>
                  <li><a href="<?php echo BASE_URL; ?>?pages=logout">
                    <i class="fa fa-sign-out"></i>Logout</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <form class="navbar-form form-inline col-lg-2 hidden-xs">
            <input class="form-control" placeholder="Search" type="text">
          </form>
        </div>
        <div class="container-fluid main-nav clearfix">
          <div class="nav-collapse">
            <ul class="nav">
              <li>
                <a class="current" href="<?php echo BASE_URL; ?>"><span aria-hidden="true" class="glyphicon glyphicon-home"></span>Dashboard</a>
              </li>
              <li class="dropdown"><a data-toggle="dropdown" href="#">
                <span aria-hidden="true" class="glyphicon glyphicon-user"></span>Students<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#">Add Student</a>
                  </li>
                  <li>
                    <a href="#">List Students</a>
                  </li>
                </ul>
              </li>
              <li class="dropdown"><a data-toggle="dropdown" href="#">
                <span aria-hidden="true" class="glyphicon glyphicon-book"></span>Assignment<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#">Upload Assignment</a>
                  </li>
                  <li>
                    <a href="#">Search By Student</a>
                  </li>

                </ul>
              </li>
              <li class="dropdown"><a data-toggle="dropdown" href="#">
                <span aria-hidden="true" class="glyphicon glyphicon-pencil"></span>Teachers<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#">Add Teacher</a>
                  </li>
                  <li>
                    <a href="#">List Teacher</a>
                  </li>

                </ul>
              </li>

              <li class="dropdown"><a data-toggle="dropdown" href="#">

                <span aria-hidden="true" class="glyphicon glyphicon-folder-open"></span>XML JSON<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="<?php echo BASE_URL; ?>?pages=xmljson&action=xml">XML</a>
                  </li>
                  <li>
                    <a href="#">Module2</a>
                  </li>
                </ul>
              </li>
              <li class="dropdown"><a data-toggle="dropdown" href="#">
                <span aria-hidden="true" class="glyphicon glyphicon-asterisk"></span>Test<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                   <a href="<?php echo BASE_URL; ?>?pages=test&action=initial">Initial Test</a>
                  </li>
                  <li>
                    <a href="<?php echo BASE_URL; ?>?pages=test&action=function">Function Tests</a>
                  </li>
                  <li>
                    <a href="#">Database Tests</a>
                  </li>
                  <li>
                    <a href="<?php echo BASE_URL; ?>?pages=test&action=formdb">Form To DB Tests</a>
                  </li>
                </ul>
              </li>
              <li class="dropdown"><a data-toggle="dropdown" href="#">
                <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>Members<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="<?php echo BASE_URL; ?>?pages=members&action=add">Add Member</a>
                  </li>
                  <li>
                    <a href="<?php echo BASE_URL; ?>?pages=members&action=list">List Members</a>
                  </li>

                </ul>
              </li>
              <li><a href="gallery.html">
                <span aria-hidden="true" class="glyphicon glyphicon-envelope"></span>Emails</a>
              </li>
              <?php if(!isset($_SESSION['user_details'])){ ?>
              <li>
                <a href="<?php echo BASE_URL; ?>?pages=login"><span class="glyphicon glyphicon-user"></span>Login</a>
              </li>
              <li>
                <a href="?pages=register"><span class="glyphicon glyphicon-book"></span>Register</a>
              </li>
            <?php } ?>
            </ul>
          </div>
        </div>
      </div>