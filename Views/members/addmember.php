<?php
  require_once('Models/Connection.php');

  $conx = new Connection();
  if(isset($_POST['input-submit']))
  {
    $fname = $_POST['input-firstname'];
    $lname = $_POST['input-lastname'];
    $email = $_POST['input-email'];
    $password = $_POST['input-password'];

    $conx->sql = "INSERT INTO members(first_name,last_name,email,password) VALUES('$fname', '$lname', '$email', '$password')";
    $conx->res = mysqli_query($conx->conn, $conx->sql)
                      or die($conx->error = mysqli_error($conx->conn));
    $conx->affRows = mysqli_affected_rows($conx->conn);
    if($conx->affRows>0)
    {
      echo "<script>window.location='?pages=members&action=list'</script>";
    }
    else
    {
      // echo "string";die;
      echo "<script>window.location='?pages=members&action=add'</script>";
    }
  }

 ?>

<div class="container">
<div class="row">
<div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info m-20" style="padding:15px; font-size:20px">
          <strong>Add</strong> Members
        </div>
        <div class="card-body card-block bg-white" style="padding:20px; height:300px;" >
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" class="form-horizontal">
            <div class="row form-group">
              <div class="col-12 col-md-6">
                <label for="firstname">First Name</label>
                <input type="text" class="form-control" placeholder="First Name" id="firstname" name="input-firstname" >
              </div>
              <div class="col-12 col-md-6">
                <label for="lastname">Last Name</label>
                <input type="text" class="form-control" placeholder="Last Name" id="lastname" name="input-lastname" >
              </div>
              </div>
              <div class="row form-group">
              <div class="col-12 col-md-6">
                <label for="email">Email</label>
                <input type="text" class="form-control" placeholder="Email" id="email" name="input-email">
              </div>
              <div class="col-12 col-md-6">
                <label for="password">Password</label>
                <input type="text" class="form-control" placeholder="********" id="password" name="input-password">
              </div>
              </div><br>
            <!-- </div> -->
            <button type="submit" class="btn btn-labeled btn-success" name="input-submit">
              <i class="glyphicon glyphicon-ok"></i> Add Members
            </button>
          <button type="reset" class="btn btn-danger btn-sm" name="form-reset1">
            <i class="glyphicon glyphicon-remove"></i> CLEAR
          </button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>