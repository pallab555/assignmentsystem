<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/">
        <html>
            <body>
                <h2>Winerys</h2>
                <table border="1">
                    <tr bgcolor="yellow">
                        <th align="left">ID</th>
                        <th align="left">Title</th>
                        <th align="left">Instructor</th>
                        <th align="left">Assignments</th>
                    </tr>
                    <xsl:for-each select="winerys/winery">
                        <tr>
                            <td>
                                <xsl:value-of select="Id"/>
                            </td>
                            <td>
                                <xsl:value-of select="Title"/>
                            </td>
                            <td>
                                <xsl:value-of select="Instructor"/>
                            </td>
                            <td>
                                <xsl:value-of select="Assignment"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>